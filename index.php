<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Jumanji</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mini.css/3.0.1/mini-nord.min.css">
    <link rel="stylesheet" href="styles.css">
</head>
<body>
<div class="wrap">
    <?php
    $csvFile = htmlspecialchars($_GET['csv']);

    if (strlen($csvFile)) : ?>
        <span class="toast">CSV-Datei aktualisiert. <a href="<?php echo $csvFile; ?>">Hier klicken zum Herunterladen.</a></span>
    <?php endif; ?>
    <?php
    $dir = 'images/';
    $preSelectedFile = htmlspecialchars($_GET['file']);
    ?>

    <div class="row">
        <div class="">
            <?php if ($handle = opendir('./' . $dir)) :
                ?>
                <h3>Datei wählen:</h3> <select id="mapSelector">
                <option></option>
                <?php while (FALSE !== ($entry = readdir($handle))) :
                    if ($entry != "." && $entry != "..") : ?>
                        <option<?php echo $preSelectedFile === $entry ? ' selected' : '' ?>><?php echo $dir . $entry; ?></option>
                    <?php endif;
                endwhile;
                closedir($handle); ?>
            </select>
            <?php else: ?>
                <h3>Keine Bilder im Pfad <?php echo $dir; ?> gefunden.</h3>
            <?php endif; ?>
        </div>
    </div>

    <div class="row" style="display: none;" id="map">
        <div>
            <br/>
            <h3>Koordinaten setzen:</h3>
        </div>
        <div class="img">
            <img src="images/map.png" draggable="false"/>
            <svg id="svg" class="svg"></svg>
        </div>
    </div>

    <form method="post" action="createCsv.php" style="display: none;" id="form">
        <fieldset>
            <legend>Koordinaten x,y</legend>
            <div class="coordinatesContainer">
            </div>
            <input type="hidden" id="coordinates" name="coordinates">
            <label for="proband">Proband</label>
            <input type="text" name="proband" id="proband"/><br />
            <button type="submit" class="primary">Zur CSV hinzufügen</button>
        </fieldset>
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="scripts.js?1"></script>
</body>
</html>
