$(function () {
    jQuery(function () {
        new DataAnalyzer().init();
    });

    function DataAnalyzer() {
        var self = this;
        var coordinatesArray = [];
        var stepLength = 50;

        this.init = function () {
            this.imgClickHandler(self);
            this.mapSelectorHandler();
            this.toastHandler();
        };

        this.imgClickHandler = function (self) {
            var img = $('.img');
            var coordinatesContainer = $('.coordinatesContainer');
            var counter = 1;
            var coordinatesInput = $('#coordinates');
            var lastCoordinate;

            img.click(function (e) {
                var parentOffset = img.offset();
                var relX = Math.round(e.pageX - parentOffset.left);
                var relY = Math.round(Math.abs(e.pageY - parentOffset.top - img.height()));
                var relYOrig = e.pageY - parentOffset.top;

                self.addDotToMap(img, relX, relYOrig, counter);
                $('#form').show();

                if (counter > 1) {
                    self.addLineBetweenDots(counter, img);
                }

                var coordinate = new Coordinate(relX, relY);

                if (coordinatesArray.length) {
                    var stepCoordinates = self.calcCoordinatesStep(lastCoordinate, coordinate, stepLength);

                    for (var i = 0; i < stepCoordinates.length; i++) {
                        coordinatesArray.push(stepCoordinates[i]);

                        var stepRelY = Math.round(Math.abs(stepCoordinates[i].y - img.height()));
                        var stepCounter = (counter - 1) + "," + (i + 1);

                        self.addDotToMap(img, stepCoordinates[i].x, stepRelY, stepCounter);
                        self.addCoordinates(stepCoordinates[i].x, stepCoordinates[i].y, stepCounter).appendTo(coordinatesContainer);
                    }

                    coordinatesArray.push(coordinate);
                } else {
                    coordinatesArray.push(coordinate);
                }

                self.addCoordinates(relX, relY, counter).appendTo(coordinatesContainer);
                coordinatesInput.val(JSON.stringify(coordinatesArray));
                lastCoordinate = coordinate;

                counter++;
            });
        };

        this.calcCoordinatesStep = function (lastCoordinate, currentCoordinate, stepLength) {
            var steps = [];
            // distance between 2 points: √( (x2 - x1)² + (y2 - y1)² )
            var distance = Math.sqrt(Math.pow((currentCoordinate.x - lastCoordinate.x), 2) + Math.pow((currentCoordinate.y - lastCoordinate.y), 2))
            var numberOfStepPoints = Math.ceil(distance / stepLength);

            if (numberOfStepPoints > 1) {
                for (var i = 1; i < numberOfStepPoints; i++) {
                    // see https://math.stackexchange.com/a/2045181
                    // x3 = x1 + d/D(x2 - x1)
                    // y3 = y1 + d/D(y2 - y1)
                    var x = Math.round(lastCoordinate.x + (stepLength * i / distance) * (currentCoordinate.x - lastCoordinate.x));
                    var y = Math.round(lastCoordinate.y + (stepLength * i / distance) * (currentCoordinate.y - lastCoordinate.y));
                    steps.push(new Coordinate(x, y));
                }
            }

            return steps;
        };

        this.addLineBetweenDots = function (counter, map) {
            var dot1 = $('#dot' + (counter - 1));
            var dot2 = $('#dot' + counter);
            var x1 = Math.round(dot1.offset().left + (dot1.width() / 2)) - map.offset().left;
            var y1 = Math.round(dot1.offset().top + (dot1.height() / 2)) - map.offset().top;
            var x2 = Math.round(dot2.offset().left + (dot2.width() / 2)) - map.offset().left;
            var y2 = Math.round(dot2.offset().top + (dot2.height() / 2)) - map.offset().top;
            var svg = $('#svg');

            svg.attr({
                'width': Math.round(map.width()),
                'height': Math.round(map.height())
            });

            var line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
            line.setAttribute('id', 'line' + counter);
            line.setAttribute('x1', x1);
            line.setAttribute('y1', y1);
            line.setAttribute('x2', x2);
            line.setAttribute('y2', y2);
            svg.append(line);
        };

        this.addDotToMap = function (map, x, y, counter) {
            var dot = $('<span style="left: ' + x + 'px; top: ' + y + 'px" id="dot' + counter + '">' + counter + '</span>');
            dot.appendTo(map);
        };

        this.addCoordinates = function (x, y, counter) {
            return $('<div></div>')
                    .append($('<label>' + counter + '. Koordinate </label>'))
                    .append('x ')
                    .append($('<input type="text" name="x" disabled />')
                            .attr('value', x))
                    .append('y ')
                    .append($('<input type="text" name="y" disabled />')
                            .attr('value', y));

        };

        this.mapSelectorHandler = function () {
            var mapSelector = $('#mapSelector');
            mapSelector.change(function () {
                var file = $(this).val();
                if (file.length > 0) {
                    var img = $('.img');
                    img.find('img').attr('src', file);
                    $('#map').show();
                }

                var proband = $('#proband');
                proband.val(file.split('/')[1]);
            });

            var urlString = window.location.href
            var url = new URL(urlString);
            var file = url.searchParams.get("f");

            if (file) {
                mapSelector.val('images/' + file).trigger('change');
            }
        };

        this.toastHandler = function () {
            $('.toast').click(function () {
                $(this).fadeOut();
            });

            setTimeout(function () {
                $('.toast').fadeOut();
            }, 7500);
        }
    }

    function Coordinate(x, y) {
        this.x = x;
        this.y = y;
    }

});
