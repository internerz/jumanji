<?php

$fileName = 'data.csv';
$coordinates = json_decode($_POST['coordinates']);
$proband = htmlspecialchars($_POST['proband']);
$numberOfMarks = 20;

if (is_array($coordinates) && $coordinates !== '' && $proband != '') {
    // add first title line to file
    if (!file_exists('./' . $fileName)) {
        $file = fopen('./' . $fileName, 'w');
        $titleString = 'Proband;';

        for ($i = 1; $i <= $numberOfMarks; $i++) {
            $titleString .= $i . 'x;';
            $titleString .= $i . 'y;';
        }

        fwrite($file, $titleString . "\n");
        fclose($file);
    }

    // add coordinates to file
    $file = fopen('./' . $fileName, 'a');
    $line = $proband . ';';

    foreach ($coordinates as $key => $coordinate) {
        $line .= $coordinate->x . ';';
        $line .= $coordinate->y . ';';
    }

    fwrite($file, $line . "\n");
    fclose($file);
}

header('Location: index.php?csv=' . $fileName . '&f=' . $proband);
